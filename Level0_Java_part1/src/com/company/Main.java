package com.company;

import com.welcome.Hello;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        System.out.print("Enter your name: ");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String userName = "%username%";
        try {
            userName = bufferRead.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }        

        Hello hello = new Hello();
        hello.setupName(userName);
        hello.welcome();
	    System.out.println("Hello, world!");
        hello.byeBye();
    }
}
