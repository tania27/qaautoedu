package com.welcome;

/**
 * Created by reznichenko on 11/22/2016.
 */
public class Hello {
    private String name;

    public void setupName(String name){
        this.name = name;
    }

    public void welcome(){
        System.out.println("Hello, " + this.name);
    }

    public void byeBye(){
        System.out.println("Bye, " + this.name);
    }
}
